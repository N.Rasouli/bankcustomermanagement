package model.service;

import model.entity.Junction;
import model.entity.LegalCustomer;
import model.repository.JpaRepository;

import java.util.List;

public class LegalCustomerService extends CommonService {
   private JpaRepository jpaRepository = new JpaRepository();

    public int save(LegalCustomer legalCustomer) {
        int resultLegalcustomer = jpaRepository.save(legalCustomer);
        if (resultLegalcustomer != 0) {
            CustomerService customerService = new CustomerService();
            int resultCustomer = customerService.save(legalCustomer.getCustomer());

            if (resultCustomer != 0) {
                JunctionService junctionService = new JunctionService();
                int junctionResutl = junctionService.save(resultLegalcustomer, resultCustomer);

                if (junctionResutl != 0) {
                    return resultLegalcustomer;
                }
            }
        }
        return 0;
    }

    public LegalCustomer findOne(String fildName, String fildValue) {
        List<LegalCustomer> legalCustomerList = jpaRepository.findOne(fildName, fildValue, LegalCustomer.class);
        try {
            if (legalCustomerList != null) {
                return new LegalCustomer().setCompanyName(legalCustomerList.get(0).getCompanyName()).setId(legalCustomerList.get(0).getId()).setEconomicCod(legalCustomerList.get(0).getEconomicCod()).setRegisterDate(legalCustomerList.get(0).getRegisterDate());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<LegalCustomer> findAll() {
        return jpaRepository.findAll(LegalCustomer.class);
    }

    public int update(LegalCustomer legalCustomer) {
        return jpaRepository.update(legalCustomer, LegalCustomer.class);
    }

    public int delete(int legalCustomerID, int customerID) {

        int resultLegalPerson = jpaRepository.delete(LegalCustomer.class, legalCustomerID);
        CustomerService customerService = new CustomerService();
        int resultCustomer = customerService.delete(customerID);
        JunctionService junctionService = new JunctionService();
        int resultJunction = junctionService.delete(legalCustomerID, customerID);

        if (resultCustomer != 0 && resultJunction != 0 && resultLegalPerson != 0) {
            return 1;
        } else return 0;
    }

    public List<LegalCustomer> findCondition(String condition) {
        return jpaRepository.findCondition(LegalCustomer.class, condition);
    }


}


