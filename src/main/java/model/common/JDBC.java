package model.common;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBC implements AutoCloseable{
    private static BasicDataSource basicDataSource=new BasicDataSource();
    static {
        basicDataSource.setUsername("HOSSEIN");//todo: read connection data from file
        basicDataSource.setPassword("HOSSEIN");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/bankCustomer?characterEncoding=UTF8");
        basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
           }
    public static Connection  getConnection()  {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            return basicDataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void close() throws Exception {

    }
}
