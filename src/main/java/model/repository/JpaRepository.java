package model.repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import model.common.JDBC;
import model.entity.*;
import model.service.RscodeService;
import org.apache.log4j.Logger;

import javax.crypto.spec.RC2ParameterSpec;

public class JpaRepository<T, FILD, ID> {
    ///  Connection connection;
    private RscodeService rscodeService = new RscodeService();

    public JpaRepository() {

    }

    public int save(T t) {
        Logger logger = Logger.getLogger(t.getClass());
        String logText = "";
        PreparedStatement preparedStatement = null;
        Connection connection = JDBC.getConnection();
        try {
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue() + t.getClass().getSimpleName());
            if (t.getClass().getSimpleName().equals(EntityTypeEnum.LegalCustomer.toString())) {
                LegalCustomer legalCustomer = (LegalCustomer) t;
                preparedStatement = connection.prepareStatement("insert into " + t.getClass().getSimpleName() + " (CompanyName,EconomicCod,RegisterDate) values (?,?,?)", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, legalCustomer.getCompanyName());
                preparedStatement.setString(2, legalCustomer.getEconomicCod());
                preparedStatement.setString(3, legalCustomer.getRegisterDate());
                logText = rscodeService.getRscodeValue(Rscode.logLegalCustomerRegister).getValue();
            } else if (t.getClass().getSimpleName().equals(EntityTypeEnum.Customer.toString())) {
                Customer customer = (Customer) t;
                preparedStatement = connection.prepareStatement("insert into " + t.getClass().getSimpleName() + " (nationalCod,Name,Family,FatherName,birthday,type)values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, customer.getNationalCod());
                preparedStatement.setString(2, customer.getName());
                preparedStatement.setString(3, customer.getFamily());
                preparedStatement.setString(4, customer.getFatherName());
                preparedStatement.setString(5, customer.getBirthDay());
                preparedStatement.setInt(6, customer.getEntityTypeEnum().getValue());
                logText = rscodeService.getRscodeValue(Rscode.CustomerRegister).getValue();
            } else if (t.getClass().getSimpleName().equals(EntityTypeEnum.Junction.toString())) {
                Junction junction = (Junction) t;
                preparedStatement = connection.prepareStatement("insert into " + t.getClass().getSimpleName() + " (legalCustomerID,customerID) values (?,?)", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, junction.getLegalCustomerID());
                preparedStatement.setInt(2, junction.getCustomerID());
                logText = rscodeService.getRscodeValue(Rscode.logJunctionRegister).getValue();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.executeUpdate();
                ResultSet tableKeys = preparedStatement.getGeneratedKeys();
                tableKeys.next();
                int key = tableKeys.getInt(1);
                logger.info(logText + key);
                connection.close();
                preparedStatement.close();
                logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue() + t.getClass().getSimpleName());
                return key;
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return 0;
    }

    public List<T> findOne(String parameterName, FILD parameterValue, Class aClass) {
        Logger logger = Logger.getLogger(aClass);
        List<T> list = null;
        try (Connection connection = JDBC.getConnection()) {
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
            PreparedStatement preparedStatement = connection.prepareStatement("select * from " + aClass.getSimpleName() + " where " + parameterName + "=" + parameterValue);
            ResultSet resultset = preparedStatement.executeQuery();

            if (aClass.getSimpleName().equals(EntityTypeEnum.Customer.toString())) {
                list = (List<T>) getCustomerFromResultSet(resultset);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindCustomer).getValue() + list.size());
            } else if (aClass.getSimpleName().equals(EntityTypeEnum.LegalCustomer.toString())) {
                list = (List<T>) getLegalCustomerFromResultSet(resultset);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindLegalCustomer).getValue() + list.size());

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return list;
        }

    }

    public List<T> findCondition(Class aClass, String condition) {
        Logger logger = Logger.getLogger(aClass);
        logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
        PreparedStatement preparedStatement = null;
        List<T> list = null;
        try (Connection connection = JDBC.getConnection()) {
            preparedStatement = connection.prepareStatement("select * from " + aClass.getSimpleName() + condition);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (aClass.getSimpleName().equals(EntityTypeEnum.Customer.toString())) {
                list = (List<T>) getCustomerFromResultSet(resultSet);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindCustomer).getValue() + list.size());
            } else if (aClass.getSimpleName().equals(EntityTypeEnum.LegalCustomer.toString())) {
                list = (List<T>) getLegalCustomerFromResultSet(resultSet);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindLegalCustomer).toString() + list.size());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return list;
        }


    }

    public List findAll(Class aClass) {
        Logger logger = Logger.getLogger(aClass);
        PreparedStatement preparedStatement = null;
        logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
        List list = null;
        try (Connection connection = JDBC.getConnection()) {
            ResultSet resultSet = null;

            if (aClass.getSimpleName().equals(EntityTypeEnum.Customer.toString())) {
                preparedStatement = connection.prepareStatement("select * from " + aClass.getSimpleName() + " where type = " + EntityTypeEnum.Customer.getValue());
                resultSet = preparedStatement.executeQuery();
                list = getCustomerFromResultSet(resultSet);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindCustomer).getValue() + list.size());
            } else if (aClass.getSimpleName().equals(EntityTypeEnum.LegalCustomer.toString())) {
                preparedStatement = connection.prepareStatement("SELECT * FROM legalcustomer legal INNER JOIN junction junc ON legal.ID = junc.legalCustomerID INNER JOIN customer cust ON junc.customerID = cust.ID");
                resultSet = preparedStatement.executeQuery();
                list = getLegalCustomerFromResultSet(resultSet);
                logger.info(rscodeService.getRscodeValue(Rscode.logFindLegalCustomer).getValue() + list.size());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return list;
        }
    }

    public int update(T t, Class aClass) {
        PreparedStatement preparedStatement = null;
        Logger logger = Logger.getLogger(aClass);
        logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
        String logText = "";
        try (Connection connection = JDBC.getConnection()) {

            if (t.getClass().getSimpleName().equals(EntityTypeEnum.LegalCustomer.toString())) {
                LegalCustomer legalCustomer = (LegalCustomer) t;
                preparedStatement = connection.prepareStatement("update " + aClass.getSimpleName() + " SET companyName = ? , registerDate = ? , economicCod = ? where id =?", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, legalCustomer.getCompanyName());
                preparedStatement.setString(2, legalCustomer.getRegisterDate());
                preparedStatement.setString(3, legalCustomer.getEconomicCod());
                preparedStatement.setInt(4, legalCustomer.getId());
                logText = rscodeService.getRscodeValue(Rscode.logUpdatelegalCustomer).getValue() + legalCustomer.getId();
            } else if (t.getClass().getSimpleName().equals(EntityTypeEnum.Customer.toString())) {
                Customer customer = (Customer) t;
                preparedStatement = connection.prepareStatement("Update " + aClass.getSimpleName() + " SET Name =? , Family=? , FatherName=? ,nationalCod=? , BirthDay=? where id=? ", Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, customer.getName());
                preparedStatement.setString(2, customer.getFamily());
                preparedStatement.setString(3, customer.getFatherName());
                preparedStatement.setString(4, customer.getNationalCod());
                preparedStatement.setString(5, customer.getBirthDay());
                preparedStatement.setInt(6, customer.getId());
                logText = rscodeService.getRscodeValue(Rscode.logUpdateCustomer).getValue() + customer.getId();
            }

            int result = preparedStatement.executeUpdate();
            logger.info(logText);
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int delete(Class aClass, ID id) {
        Logger logger = Logger.getLogger(aClass);
        logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
        PreparedStatement preparedStatement = null;
        try (Connection connection = JDBC.getConnection()) {
            preparedStatement = connection.prepareStatement("delete from " + aClass.getSimpleName() + " where id= " + id);
            int result = preparedStatement.executeUpdate();
            logger.info(rscodeService.getRscodeValue(Rscode.logDelete).getValue() + id);
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int deleteByCondition(Class aClass, String condition) {
        Logger logger = Logger.getLogger(aClass);
        logger.debug(rscodeService.getRscodeValue(Rscode.logDebugStart).getValue());
        PreparedStatement preparedStatement = null;
        try (Connection connection = JDBC.getConnection()) {
            preparedStatement = connection.prepareStatement("delete from " + aClass.getSimpleName() + condition);
            int result = preparedStatement.executeUpdate();
            logger.info(rscodeService.getRscodeValue(Rscode.logDeleteByCondition).getValue() + condition);
            logger.debug(rscodeService.getRscodeValue(Rscode.logDebugEnd).getValue());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private List<LegalCustomer> getLegalCustomerFromResultSet(ResultSet resultSet) {
        List<LegalCustomer> companies = new ArrayList<>();
        try {

            if (resultSet != null) {

                while (resultSet.next()) {
                    LegalCustomer legalCustomer = new LegalCustomer().setCompanyName(resultSet.getString(2)).setId(resultSet.getInt(1)).setEconomicCod(resultSet.getString(4)).setRegisterDate(resultSet.getString(3));
                    companies.add(legalCustomer);

                    if (isThereColumnInResultSet(resultSet, "name")) {
                        Customer customer = new Customer().setName(resultSet.getString("name")).setBirthDay(resultSet.getString("birthDay")).setFamily(resultSet.getString("family")).setFatherName(resultSet.getString("fatherName")).setNationalCod(resultSet.getString("nationalCod")).setId(Integer.valueOf(resultSet.getString("customerID")));
                        legalCustomer.setCustomer(customer);
                    }

                }

                return companies;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isThereColumnInResultSet(ResultSet rs, String column) {
        try {
            rs.findColumn(column);
            return true;
        } catch (SQLException sqlex) {
            return false;
        }

    }

    private List<Customer> getCustomerFromResultSet(ResultSet resultSet) {
        List<Customer> people = new ArrayList<>();
        try {

            if (resultSet != null) {

                while (resultSet.next()) {
                    Customer customer = new Customer().setName(resultSet.getString("name")).setBirthDay(resultSet.getString("birthDay")).setFamily(resultSet.getString("family")).setFatherName(resultSet.getString("fatherName")).setNationalCod(resultSet.getString("nationalCod")).setId(Integer.valueOf(resultSet.getString("id")));
                    people.add(customer);
                }

                return people;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
