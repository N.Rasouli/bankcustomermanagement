/*create table junctionTable(
        id int unsigned AUTO_INCREMENT PRIMARY KEY,
        legalCustomerID  int not null,
        customerID int not null

        )*/
package model.entity;

public class Junction {
    private int id;
    private int LegalCustomerID;
    private int customerID;

    public int getId() {
        return id;
    }

    public Junction setId(int id) {
        this.id = id;
        return this;
    }

    public int getLegalCustomerID() {
        return LegalCustomerID;
    }

    public Junction setLegalCustomerID(int legalCustomerID) {
        LegalCustomerID = legalCustomerID;
        return this;
    }

    public int getCustomerID() {
        return customerID;
    }

    public Junction setCustomerID(int customerID) {
        this.customerID = customerID;
        return this;
    }
}
