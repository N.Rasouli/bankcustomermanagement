package model.entity;

/* sql command for create table
    create table legalCustomer(
    id int unsigned AUTO_INCREMENT PRIMARY KEY,
        companyName  varchar(100) not null,
        registerDate varchar(50),
       economicCod varchar(100) not null UNIQUE
)
 */
public class LegalCustomer {
    private int id;
    private String companyName;
    private String registerDate;
    private String economicCod;
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public LegalCustomer setCustomer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public int getId() {
        return id;
    }

    public LegalCustomer setId(int id) {
        this.id = id;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public LegalCustomer setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public LegalCustomer setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
        return this;
    }

    public String getEconomicCod() {
        return economicCod;
    }

    public LegalCustomer setEconomicCod(String economicCod) {
        this.economicCod = economicCod;
        return this;
    }
}
