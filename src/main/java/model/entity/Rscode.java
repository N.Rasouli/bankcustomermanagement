package model.entity;

public enum Rscode {
    done,
    ERROR,
    CustomerRegister,
    logDebugStart,
    logDebugEnd,
    logJunctionRegister,
    logLegalCustomerRegister,
    logFindCustomer,
    logFindLegalCustomer,
    logUpdateCustomer,
    logUpdatelegalCustomer,
    logDelete,
    logDeleteByCondition
}
