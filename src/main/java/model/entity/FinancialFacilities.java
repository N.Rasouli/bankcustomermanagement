package model.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name="financialFacilities")
@Table(name="FinancialFacilities")
public class FinancialFacilities {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id", columnDefinition = "NUMBER")
    private int id;
   @Column(name = "name",columnDefinition = "NVARCHAR2(200)")
    private  String name;
   @Column(name = "interestRate",columnDefinition = "NUMBER")
    private  int interestRate;
   @OneToMany(cascade = CascadeType.ALL)
    private  List<ConditionFacilities> conditionFacilitiesList;
@Version
    private int version;

    public int getId() {
        return id;
    }

    public FinancialFacilities setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public FinancialFacilities setName(String name) {
        this.name = name;
        return this;
    }

    public int getInterestRate() {
        return interestRate;
    }

    public FinancialFacilities setInterestRate(int interestRate) {
        this.interestRate = interestRate;
        return this;
    }

    public List<ConditionFacilities> getConditionFacilitiesList() {
        return conditionFacilitiesList;
    }

    public FinancialFacilities setConditionFacilitiesList(List<ConditionFacilities> conditionFacilitiesList) {
        this.conditionFacilitiesList = conditionFacilitiesList;
        return this;
    }
}
