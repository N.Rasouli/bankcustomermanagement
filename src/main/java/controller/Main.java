package controller;

import model.entity.ConditionFacilities;
import model.entity.FinancialFacilities;
import model.service.FinancialFacilitiesService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FinancialFacilities financialFacilities = new FinancialFacilities();
        financialFacilities.setInterestRate(1);
        financialFacilities.setName("t");
        ConditionFacilities conditionFacilities = new ConditionFacilities();
        conditionFacilities.setMaximumAmount(1);
        conditionFacilities.setMaximumTime(1);
        conditionFacilities.setMinimumAmount(1);
        conditionFacilities.setMinimumTime(1);
        conditionFacilities.setName("c1");
        List<ConditionFacilities> conditionFacilities1 = new ArrayList<>();
        conditionFacilities1.add(conditionFacilities);
        financialFacilities.setConditionFacilitiesList(conditionFacilities1);
        FinancialFacilitiesService financialFacilitiesService=new FinancialFacilitiesService();
        financialFacilitiesService.save(financialFacilities);
    }
}
