package test;

import model.service.RscodeService;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;

public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext servletContext = servletContextEvent.getServletContext();
        String contextpath = servletContext.getRealPath("/");
       // PropertyConfigurator.configure(contextpath + "\\resources\\log4j.properties");
        System.setProperty("logfile.name", contextpath + "D:\\log.log");

        ServletContext context = servletContextEvent.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;

        PropertyConfigurator.configure(fullPath);
        RscodeService rscodeService=new RscodeService();
        rscodeService.getRscodeValues(contextpath);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
