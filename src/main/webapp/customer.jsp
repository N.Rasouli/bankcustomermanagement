<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.entity.Customer" %>
<%@ page import="java.util.List" %>
<%@ page import="static jdk.nashorn.internal.objects.Global.print" %>
<%@ page import="static jdk.nashorn.internal.objects.Global.print" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:requestEncoding value="UTF-8"/>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>سامانه مدیریت کاربران</title>

    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/resources/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="/resources/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/resources/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/resources/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/resources/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/resources/css/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link href="/resources/css/popUp.css" rel="stylesheet">
    <link href="/resources/css/custom.css" rel="stylesheet">
    <link href="/resources/css/formMulti.css" rel="stylesheet">
    <link href="/resources/css/calendar-blue.css" rel="stylesheet">
    <%@include file="menu.jsp" %>
</head>

<body>

<div id="wrapper">
    <div id="page-wrapper" style="min-height: 334px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">مدیریت کاربران</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        پنل مدیریت
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="${activeBaseLI}">
                                <a href="#home" data-toggle="tab"><i class="fa fa-certificate" aria-hidden="true">
                                    معرفی </i></a></li>
                            <li class="${activeRegisterLI}">

                                <a href="#profile" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"> ثبت کاربر
                                    جدید </i></a></li>
                            <li class="${activeUpdateLI}">
                                <a href="#messages" data-toggle="tab"><i class="fa fa-list" aria-hidden="true"> اصلاح و
                                    حذف اطلاعات </i></a></li>
                            <li class="${activeSearchLI}"><a href="#search" data-toggle="tab"><i class="fa fa-search"
                                                                                                 aria-hidden="true">
                                جستجو پیشرفته </i></a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade ${activeBasePanel} " id="home">
                                <h4>معرفی</h4>
                                <p>اولین قسم از اقسام شخصیت در حقوق ، شخص حقیقی است . در تعریف شخص حقیقی می توان به زبان
                                    ساده گفت که شخص حقیقی یا شخصیت حقیقی خود ما هستیم . در واقع همه افراد بشر ، شخص
                                    حقیقی محسوب می شوند . انسان از زمانی که متولد می شود تا زمانی که از دنیا می رود
                                    دارای شخصیت حقیقی است . یعنی طبق تعریفی که از شخصیت کردیم ، قانون به او شایستگی این
                                    را داده است تا بتواند صاحب یک سری حقوق و تکالیف شود . </p>
                            </div>
                            <div class="tab-pane fade ${activeRegisterPanel} " id="profile">
                                <!-- Sign up form -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="signup-content">
                                            <div class="signup-form">
                                                <h2 class="form-title">ثبت اطلاعات</h2>
                                                <form method="POST" class="register-form" id="register-form"
                                                      action="/customer/customerService.do?action=save">
                                                    <div class="form-group">
                                                        <label for="name"><i
                                                                class="zmdi zmdi-account material-icons-name"></i></label>
                                                        <input type="text" oninput="this.className = ''" name="name"
                                                               id="name" placeholder="نام" onkeypress="text('name')"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="family"><i class="zmdi zmdi-email"></i></label>
                                                        <input type="text" oninput="this.className = ''" name="family"
                                                               id="family"
                                                               placeholder="نام خانوادگی" onkeypress="text('family')"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="fatherName"><i class="zmdi zmdi-lock"></i></label>
                                                        <input type="text" oninput="this.className = ''"
                                                               name="fatherName" id="fatherName"
                                                               placeholder="نام پدر" onkeypress="text('fatherName')"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="birthDay"><i
                                                                class="zmdi zmdi-lock-outline"></i></label>

                                                        <input placeholder="تاریخ تولد" oninput="this.className = ''"
                                                               id="birthDay" name="birthDay" type="text"
                                                               onkeydown="return false;"
                                                               onselect="dateJalali('birthDay')">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nationalCod"><i class="zmdi zmdi-lock-outline"></i></label>
                                                        <input type="text" name="nationalCod" id="nationalCod"
                                                               placeholder="کدملی" oninput="this.className = ''"
                                                               onkeypress="text('nationalCod')"/>
                                                    </div>
                                                    <div class="form-group form-button">
                                                        <button type="button" name="submitForm"
                                                                onclick="validateCustomer('register-form')">
                                                            ثبت اطلاعات
                                                        </button>

                                                    </div>
                                                    <div class="form-group ">
                                                        <label id="response"
                                                               class="form-submit"><%=request.getAttribute("reponseRegister")%>
                                                        </label>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="signup-image">
                                                <figure><img
                                                        src="<%=request.getContextPath()%>/resources/images/signup-image.jpg">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade ${activeUpdatePanel} " id="messages">
                                <h4>به روز رسانی اطلاعات</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-striped table-bordered table-hover dataTable no-footer"
                                               id="dataTables-example" role="grid"
                                               aria-describedby="dataTables-example_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    شمااره مشتری
                                                </th>
                                                <th class="sorting_asc" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    نام
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Browser: activate to sort column ascending">
                                                    نام خانوادگی
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    نام پدر
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    شماره ملی
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: auto;"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    تاریخ تولد
                                                </th>
                                                <th class="sorting" tabindex="0"
                                                    aria-controls="dataTables-example" rowspan="1"
                                                    colspan="1" style="width: 90px;"
                                                    aria-label="CSS grade: activate to sort column ascending">
                                                    عملیات
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${requestScope.list}" var="p">
                                                <form method="post" action="/customer/customerService.do?action=delete">

                                                    <input type="hidden" name="id" value="${p.id}"/>
                                                    <tr class="gradeA odd" role="row">
                                                        <td class="sorting_1">${p.id}</td>
                                                        <td class="sorting_1">${p.name}</td>
                                                        <td class="center"> ${p.family}</td>
                                                        <td class="center">${p.fatherName}</td>
                                                        <td class="center">${p.nationalCod}</td>
                                                        <td class="center">${p.birthDay}</td>
                                                        <td class="center">
                                                            <button class="btn btn-danger" type="button"
                                                                    onclick="removePerson(${p.id})"
                                                            ><i class="fa fa-trash"></i></button>
                                                            <button class="btn btn-success" id="myBtn1"
                                                                    onclick="openBox('${p.id}','${p.name}','${p.family}','${p.fatherName}','${p.nationalCod}','${p.birthDay}')"
                                                                    type="button"><i class="fa fa-refresh"></i>
                                                            </button>

                                                        </td>
                                                        </td>

                                                    </tr>
                                                </form>
                                            </c:forEach>

                                            </tbody>
                                        </table>
                                        <!-- The Modal -->
                                        <div id="customerModal" class="modal">

                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <span class="close">&times;</span>
                                                <div class="modal-body">
                                                    <h2 class="form-title">به روز رسانی اطلاعات</h2>

                                                    <form method="post"
                                                          action="/customer/customerService.do?action=update"
                                                          id="update-form">
                                                        <input type="hidden" id="uid1" name="id"/>


                                                        <div class="form-group">
                                                            <label for="name">نام</label>
                                                            <input type="text" oninput="this.className = ''" name="name"
                                                                   id="uname" placeholder="نام"
                                                                   onkeypress="text('uname')"/>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="birthDay">تاریخ تولد</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="birthDay"
                                                                   id="ubirthDay" placeholder="تاریخ تولد"
                                                                   onselect="dateJalali('ubirthDay')"
                                                                   onkeydown="return false;"
                                                                   oninput="this.className = ''""/>
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="family">نام خانوادگی</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="family"
                                                                   id="ufamily" placeholder="نام خانوادگی"
                                                                   onkeypress="text('ufamily')"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="nationalCod">کدملی</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="nationalCod"
                                                                   id="unationalCod" placeholder="کدملی"
                                                                   onkeypress="text('unationalCod')"/>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fatherName">نام پدر</label>
                                                            <input type="text" oninput="this.className = ''"
                                                                   name="fatherName"
                                                                   id="ufatherName" placeholder="نام پدر"
                                                                   onkeypress="text('ufatherName')"/>
                                                        </div>
                                                        <div class="form-group form-button">

                                                            <button type="button"
                                                                    onclick="validateCustomer('update-form')">
                                                                به روز رسانی
                                                            </button>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group ">
                                            <label name="response" id="responseUpdate"
                                                   class="form-submit"><%=request.getAttribute("responseUpdate")%>
                                            </label>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="tab-pane fade ${activeSearchPanel}" id="search">

                                <h4>جستجو پیشرفته</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table>
                                            <form method="post" action="/customer/customerService.do?action=search">
                                                <div class="divRow">
                                                    <div class="divCell"><label> و </label><input type="radio"
                                                                                                  name="conditionName"
                                                                                                  value="and"/></div>
                                                    <div class="divCell"><label> یا </label><input type="radio"
                                                                                                   name="conditionName"
                                                                                                   value="or"/></div>
                                                    <div class="divCell-text">نام</div>

                                                    <div class="divCell"><input type="text" class="form-control"
                                                                                id="nameSearch"
                                                                                onkeypress="text('nameSearch')"
                                                                                name="name" placeholder="نام..."/></div>

                                                </div>
                                                <div class="divRow">
                                                    <div class="divCell"><label> و </label><input type="radio"
                                                                                                  name="conditionBirthDay"
                                                                                                  value="and"/></div>
                                                    <div class="divCell"><label> یا </label><input type="radio"
                                                                                                   name="conditionBirthDay"
                                                                                                   value="or"/></div>
                                                    <div class="divCell-text"> تاریخ تولد</div>
                                                    <div class="divCell"><input placeholder="تاریخ تولد..."
                                                                                class="form-control" id="birthDaySearch"
                                                                                type="text" name="birthDay"
                                                                                onkeydown="return false;"
                                                                                onselect="dateJalali('birthDaySearch')">
                                                    </div>

                                                </div>

                                                <div class="divRow">
                                                    <div class="divCell"><label> و </label><input type="radio"
                                                                                                  name="conditionFamily"
                                                                                                  value="and"/></div>
                                                    <div class="divCell"><label> یا </label><input type="radio"
                                                                                                   name="conditionFamily"
                                                                                                   value="or"/></div>
                                                    <div class="divCell-text">نام خانوادگی</div>
                                                    <div class="divCell"><input type="text" class="form-control"
                                                                                id="familySearch"
                                                                                name="family"
                                                                                onkeypress="text('family')"
                                                                                placeholder="نام خانوادگی..."/></div>

                                                </div>
                                                <div class="divRow">
                                                    <div class="divCell"><label> و </label><input type="radio"
                                                                                                  name="conditionNationalCod"
                                                                                                  value="and"/></div>
                                                    <div class="divCell"><label> یا </label><input type="radio"
                                                                                                   name="conditionNationalCod"
                                                                                                   value="or"/></div>
                                                    <div class="divCell-text">کدملی</div>
                                                    <div class="divCell"><input type="text" class="form-control"
                                                                                name="nationalCod"
                                                                                id="nationalCodSearch"
                                                                                onkeypress="text('nationalCodSearch')"
                                                                                placeholder="کدملی..."/>
                                                    </div>

                                                </div>
                                                <div class="divRow">
                                                    <div class="divCell"><label> و </label><input type="radio"
                                                                                                  name="conditionFatherName"
                                                                                                  value="and"/></div>
                                                    <div class="divCell"><label> یا </label><input type="radio"
                                                                                                   name="conditionFatherName"
                                                                                                   value="or"/></div>
                                                    <div class="divCell-text">نام پدر</div>
                                                    <div class="divCell"><input type="text" class="form-control"
                                                                                name="fatherName" id="fatherNameSearch"
                                                                                onkeypress="text('fatherNameSearch')"
                                                                                placeholder="نام پدر..."/>
                                                    </div>

                                                </div>
                                                <div class="divRow">

                                                    <div class="divCell">
                                                        <input type="button" value="جستجو"
                                                               onclick="submitForm(this.form)">
                                                    </div>
                                                </div>


                                            </form>
                                        </table>
                                    </div>
                                </div>
                                <div id="results">

                                </div>

                                <!-- /.panel-body -->
                            </div>


                        </div>
                    </div>

                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
</div>

<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<!-- jQuery Version 1.11.0 -->
<script src="/resources/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/resources/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/resources/js/metisMenu/metisMenu.min.js"></script>


<script src="/resources/js/jquery/jquery.dataTables.min.js"></script>
<script src="/resources/js/bootstrap/dataTables.bootstrap.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/resources/js/sb-admin-2.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });

</script>
<script>
    function removePerson(id) {
        if (confirm('ایا برای حذف اطمینان کافی دارید؟'))
            window.location = "/customer/customerService.do?action=delete&id=" + id;
    }
</script>
<script src="/resources/js/popUp.js"></script>
<script src="/resources/js/searchTableCustomer.js"></script>
<script src="/resources/js/jalaliCalender/jalali.js"></script>
<script src="/resources/js/jalaliCalender/calendar.js"></script>
<script src="/resources/js/jalaliCalender/calendar-setup.js"></script>
<script src="/resources/js/jalaliCalender/lang/calendar-fa.js"></script>
<script src="/resources/js/formCheckCustome.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="/resources/js/html5shiv.js"></script>
<script src="/resources/js/respond.min.js"></script>
<![endif]-->
</body>
</html>